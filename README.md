---
tags:
- generated_from_trainer
- financial-tweets-sentiment-analysis
- sentiment-analysis
- generated_from_trainer
- financial
- stocks
- sentiment
datasets:
- zeroshot/twitter-financial-news-sentiment
metrics:
- accuracy
- f1
- precision
- recall
widget:
- text: "$LOW - Lowe's racks up another positive rating despite recession risk"
  example_title: "Bullish Sentiment"
- text: "$HNHAF $HNHPD $AAPL - Trendforce cuts iPhone estimate after Foxconn delay"
  example_title: "Bearish Sentiment"
- text: "Coin Toss: Morgan Stanley Raises Tesla Bull Case To $500, Keeps Bear Case At $10"
  example_title: "Neutral Sentiment"
model-index:
- name: finbert-tone-finetuned-fintwitter-classification
  results:
  - task:
      name: Text Classification
      type: text-classification
    dataset:
      name: twitter-financial-news-sentiment
      type: finance
    metrics:
    - type: F1
      name: F1
      value: 0.8838
    - type: accuracy
      name: accuracy
      value: 0.8840
---

<!-- This model card has been generated automatically according to the information the Trainer had access to. You
should probably proofread and complete it, then remove this comment. -->

# finbert-tone-finetuned-fintwitter-classification

This model is a fine-tuned version of [yiyanghkust/finbert-tone](https://huggingface.co/yiyanghkust/finbert-tone) on [Twitter Financial News](https://huggingface.co/datasets/zeroshot/twitter-financial-news-sentiment) dataset.
It achieves the following results on the evaluation set:
- Loss: 1.4078
- Accuracy: 0.8840
- F1: 0.8838
- Precision: 0.8838
- Recall: 0.8840

## Model description

Model determines the financial sentiment of given tweets. Given the unbalanced distribution of the class labels, the weights were adjusted to pay attention to the less sampled labels which should increase overall performance..


## Intended uses & limitations

More information needed

## Training and evaluation data

More information needed

## Training procedure

### Training hyperparameters

The following hyperparameters were used during training:
- learning_rate: 2e-05
- train_batch_size: 16
- eval_batch_size: 16
- seed: 42
- optimizer: Adam with betas=(0.9,0.999) and epsilon=1e-08
- lr_scheduler_type: linear
- num_epochs: 20
- mixed_precision_training: Native AMP

### Training results

| Training Loss | Epoch | Step  | Validation Loss | Accuracy | F1     | Precision | Recall |
|:-------------:|:-----:|:-----:|:---------------:|:--------:|:------:|:---------:|:------:|
| 0.6385        | 1.0   | 597   | 0.3688          | 0.8668   | 0.8693 | 0.8744    | 0.8668 |
| 0.3044        | 2.0   | 1194  | 0.3994          | 0.8744   | 0.8726 | 0.8739    | 0.8744 |
| 0.1833        | 3.0   | 1791  | 0.6212          | 0.8781   | 0.8764 | 0.8762    | 0.8781 |
| 0.1189        | 4.0   | 2388  | 0.8370          | 0.8740   | 0.8743 | 0.8748    | 0.8740 |
| 0.0759        | 5.0   | 2985  | 0.9107          | 0.8807   | 0.8798 | 0.8796    | 0.8807 |
| 0.0291        | 6.0   | 3582  | 0.9711          | 0.8836   | 0.8825 | 0.8821    | 0.8836 |
| 0.0314        | 7.0   | 4179  | 1.1305          | 0.8819   | 0.8811 | 0.8812    | 0.8819 |
| 0.0217        | 8.0   | 4776  | 1.0190          | 0.8811   | 0.8813 | 0.8816    | 0.8811 |
| 0.0227        | 9.0   | 5373  | 1.1940          | 0.8844   | 0.8832 | 0.8838    | 0.8844 |
| 0.0156        | 10.0  | 5970  | 1.2595          | 0.8752   | 0.8768 | 0.8801    | 0.8752 |
| 0.0135        | 11.0  | 6567  | 1.1931          | 0.8760   | 0.8768 | 0.8780    | 0.8760 |
| 0.009         | 12.0  | 7164  | 1.2154          | 0.8857   | 0.8852 | 0.8848    | 0.8857 |
| 0.0058        | 13.0  | 7761  | 1.3874          | 0.8748   | 0.8759 | 0.8776    | 0.8748 |
| 0.009         | 14.0  | 8358  | 1.4193          | 0.8740   | 0.8754 | 0.8780    | 0.8740 |
| 0.0042        | 15.0  | 8955  | 1.2999          | 0.8807   | 0.8800 | 0.8796    | 0.8807 |
| 0.0028        | 16.0  | 9552  | 1.3428          | 0.8802   | 0.8805 | 0.8817    | 0.8802 |
| 0.0029        | 17.0  | 10149 | 1.3959          | 0.8807   | 0.8807 | 0.8810    | 0.8807 |
| 0.0022        | 18.0  | 10746 | 1.4149          | 0.8827   | 0.8823 | 0.8824    | 0.8827 |
| 0.0037        | 19.0  | 11343 | 1.4078          | 0.8840   | 0.8838 | 0.8838    | 0.8840 |
| 0.001         | 20.0  | 11940 | 1.4236          | 0.8823   | 0.8823 | 0.8825    | 0.8823 |


### Framework versions

- Transformers 4.25.1
- Pytorch 1.13.0+cu116
- Datasets 2.8.0
- Tokenizers 0.13.2
